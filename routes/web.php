<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/mard', 'HumanController@index');
Route::resource('planets', 'PlanetController');

Route::get('/', function () {
    return view('welcome');
});
Route::get('/choose-forum', 'DevelopersController@index');
Route::get('/dev-forum', 'DevelopersController@devForum')->middleware('isDev')->name('dev-forum');
Route::get('/inp-list', 'AdminController@index');
Route::get('/log-list', 'AdminController@admin')->middleware('isAdmin')->name('log-list');
Route::get('/phones', 'PhoneController@index');
Route::get('/country', 'PhoneController@usersCountry');