<form action="{{ route('planets.update', ['id'=>$planet->id]) }}" method="post">
    {{@csrf_field()}}
    {{@method_field('PUT')}}
    <div>
        <label>Update Planets</label>
        <input type="text" name="name" value="{{$planet->name}}">
        <input type="text" name="size" value="{{$planet->size}}">
        <input type="text" name="cordinate" value="{{$planet->cordinate}}">

    </div>
    <button type="submit">
        Update Planet
    </button>
</form>
