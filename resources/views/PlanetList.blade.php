<h1>Planets</h1>
<h2><a href="{{route('planets.create')}}">Add new planet</a></h2>

@foreach ($planets as $planet)
    <p>{{$planet['id']}} {{$planet['name']}}</p>
    <a href="{{route('planets.edit', ['id'=>$planet->id])}}">Edit</a>
    <form action="{{route('planets.destroy', ['id'=>$planet->id])}}" method="post">
        {{@csrf_field()}}
        <input type="hidden" name="_method" value="DELETE">
        <button type="submit">
            DELETE
        </button>
    </form>
@endforeach