<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->name, $request->email)&& $request->name == 'Hamo' && $request->email == 'hamo@gmail'  ) {
            return $next($request);
        }
    }
}
