<?php

namespace App\Http\Middleware;

use Closure;

class Proffesion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->profession)&& $request->profession== 'developer') {
            return $next($request);
        }
    }
}
