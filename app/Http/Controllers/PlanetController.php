<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Planet;

class PlanetController extends Controller
{

    public function index()
    {
        $planets = Planet::all();
        return view('PlanetList', compact('planets'));

    }


    public function create()
    {
        return view('AddPlanet');
    }


    public function store(Request $request)
    {
       $input =  $request->except('_token');
       $new_planet  = new Planet();
       $new_planet->fill($input);

       if($new_planet->save()) {

           return redirect('planets');
       } else {return redirect('back');}
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $planet = Planet::findOrFail($id);
        return view('PlanetUpdate', compact('planet'));
    }


    public function update(Request $request, $id)
    {
        $inputs = $request->except(['_token_', '_method']);
        $update_planets = Planet::find($id);
        $update_planets->fill($inputs);
        $update_planets->update();


    }


    public function destroy($id)
    {
        $del_planet = Planet::find($id);
        $del_planet->delete();
    }
}
