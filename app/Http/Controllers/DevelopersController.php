<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DevelopersController extends Controller
{
    public function index(){
        return view('choose-forum');
}
public function devForum (Request $request){
        $name = $request->name;
        $lastname = $request->lastname;
        $prof = $request->profession;
        return view('devForum', [
            'name' => $name,
            'lastname' => $lastname,
            'prof' => $prof

        ]);
}
}
